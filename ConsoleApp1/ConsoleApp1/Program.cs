﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;
using System.IO;
using Vizz.core.data;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // PART 1
            // First let's put together a person based on Mike's Vizz Data Object and use JsonConvert.Serialize
            // Write the output to vdoTest.json

            VizzDataObject vdoPerson = new VizzDataObject();
            VizzDataProperty friendly = new BoolProperty
            {
                PropertyName = "Is a friend",
                PropertyType = VizzDataProperty.Type.booleanProp,
                Value = true
            };
            vdoPerson.Properties.Add(friendly);

            VizzDataObject personName = new VizzDataObject();
            personName.Properties.Add(new StringProperty
            {
                PropertyName = "First Name",
                PropertyType = VizzDataProperty.Type.stringProp,
                Value = "John",
            });
            personName.Properties.Add(new StringProperty
            {
                PropertyName = "Middle Name",
                PropertyType = VizzDataProperty.Type.stringProp,
                Value = "Harvey",
            });
            personName.Properties.Add(new StringProperty
            {
                PropertyName = "Last Name",
                PropertyType = VizzDataProperty.Type.stringProp,
                Value = "Doe",
            });

            vdoPerson.SecondaryObjects.Add(new VizzDataObject.VizzDataRelationship
            {
                relatedObject = personName,
                relationshipName = "HAS_A_NAME",
                RelationshipType = "SECONDARY",
                Direction = "TO",
                relationshipProperties = new List<VizzDataProperty>
                {
                    new BoolProperty { PropertyName= "privateFlag", PropertyType = VizzDataProperty.Type.booleanProp, Value = false},
                    new StringProperty {PropertyName="purpose", Value="WORK", PropertyType= VizzDataProperty.Type.stringProp }
                }
            });

            string vdoJSON = JsonConvert.SerializeObject(vdoPerson, Formatting.Indented);
            //Console.WriteLine(vdoJSON);
            SaveJSONToFile(vdoJSON, "vdoTest.json");

            //PressAnyKeyToContinue();

            // Part 1.5
            // Read a JSON from another source into a string, turn it into a JObject, do some queries on that JOBject

            string jostring = File.ReadAllText("vdoTest.json");
            JObject joFromSource = JObject.Parse(jostring);

            DisplayPropertiesAndTypes(joFromSource);

            PressAnyKeyToContinue();

            // PART 2
            // Create a conversion from VDO to a Newtonsoft JObject
            // Display it to the console and write it out to joPersonTest.json

            var convertedFromVdoPerson = vdoPerson.ToJObject();

            Console.WriteLine(convertedFromVdoPerson.ToString());
            SaveJSONToFile(convertedFromVdoPerson.ToString(), "convertedVdoTest.json");

            PressAnyKeyToContinue();

            // PART 3
            // Can JObject replace / supplant the need for our own VDO object for 2.0?
            // It is VERY versatile, tracks the type of each property (which can itself hold a JObject)
                       
            JObject Person = new JObject();

            JObject ObjectData = new JObject();
            JObject Metadata = new JObject();
            Metadata["templateType"] = "Person";
            Metadata["objectid"] = new Guid();

            JObject PersonName = new JObject();
            PersonName["First Name"] = "John";
            PersonName["Middle Name"] = "Harvey";
            PersonName["Last Name"] = "Doe";

            ObjectData["Person Name"] = PersonName;
            ObjectData["Test bool"] = true;
            ObjectData["Test Number"] = 8000;
            ObjectData["Test Date"] = new DateTime(2019, 5, 30);
            ObjectData["Test float"] = 1.4124;
            object testObj = new { name = "testObject", testProp1 = true, testProp2 = "blah", testProp3 = 42 };
            ObjectData["Test Object"] = JObject.FromObject(testObj);
            
            ObjectData["Test Array"] = new JArray(new List<string> { "alpha", "bravo", "charlie", "delta" });

            Person["Object Data"] = ObjectData;
            Person["Metadata"] = Metadata;
            
            DisplayPropertiesAndTypes(Person);

            PressAnyKeyToContinue();

            return;
            JObject joPerson = new JObject();

            joPerson["Is a Friend"] = true;

            JObject joPersonName = new JObject();
            joPersonName["First Name"] = "John";
            joPersonName["Middle Name"] = "Harvey";
            joPersonName["Last Name"] = "Doe";

            joPerson["Person Name"] = joPersonName;
            System.Console.WriteLine(joPerson.ToString());

            SaveJSONToFile(joPerson.ToString(), "joPersonTest.json");

            PressAnyKeyToContinue();

            DisplayPropertiesAndTypes(joPerson);

            PressAnyKeyToContinue();

            return;

            joPerson = CreateSamplePerson();
            System.Console.WriteLine(joPerson.ToString());

            //DisplayProperties(person);

            PressAnyKeyToContinue();

            return;

            return;
            Console.WriteLine(joPerson.ToString());



            //I can test if a property exists
            TestPropertyExists(joPerson, "Height");


            //I can query a property for it's type
            if (joPerson["Name"].Type == JTokenType.Object)
            {
                Console.WriteLine("Name is an object");
            }


            //If it's helpful, I can add a separate list of user-defined properties, but it is not necessary
            //person.Add("User-Defined Properties", new JObject());
            //if(person["User-Defined Properties"].Type == JTokenType.Object)
            //{
            //    ((JObject)person["User-Defined Properties"]).Add("Age", 36);
            //}

            //I can do a quick and dirty string match:
            var querystring = "wal";
            if (joPerson.ToString().ToUpper().Contains(querystring.ToUpper()))
            {
                Console.WriteLine("Person is a match for the query string: " + querystring);
            }

            //I can convert this to a Json string
            Console.WriteLine("Person as a json string: \n" + joPerson.ToString());

            //I can retrieve any specific property simply from it's name
            Console.WriteLine("Person's Middle Name: " + joPerson["Middle Name"]);

            //I can also define schemas (or "templates")
            JSchema personSchema = CreatePersonSchema();
            DisplaySchema(personSchema);
            ValidateObjectAgainstSchema(joPerson, personSchema);

            //I can iterate over a schema to find out what fields I need to present to a user to accomodate that schema:
            SchemaForm(personSchema);
            JObject employmentRelationship = CreateEmploymentRelationship(joPerson);
            SaveJSONToFile(joPerson.ToString(), "person.json");
            //fstream = File.CreateText("personschema.json");
            //fstream.WriteLine(personSchema.ToString());
            //fstream.Close();
            //fstream = File.CreateText("relationship.json");
            //fstream.WriteLine(employmentRelationship.ToString());
            //fstream.Close();
            PressAnyKeyToContinue();
        }

        private JSchema GenerateTaskTemplate()
        {
            throw new NotImplementedException();
        }

        private static void GenerateObject()
        {
            //VizzObject vo = new VizzObject();
            //VizzComponent nameComponent = new VizzComponent("PersonName", new List<string> { "First Name", "Last Name" });
            //vo.ObjectComponents.Add(nameComponent);
        }

        private static void TestPropertyExists(JObject jo, string propName)
        {
            if (jo[propName] == null)
            {
                Console.WriteLine("{0} Property does not exist", propName);
            }
            else
            {
                Console.WriteLine("{0} property exists", propName);
            }
        }

        private static void SaveJSONToFile(string jsonString, string filename)
        {
            var fstream = File.CreateText(filename);
            fstream.WriteLine(jsonString);
            fstream.Close();
        }

        private static JObject CreateEmploymentRelationship(JObject person)
        {
            JObject employmentRelationship = new JObject();
            employmentRelationship.Add("Relationship Name", "Employed By");
            employmentRelationship.Add("From Object ID", person["GUID"]);
            employmentRelationship.Add("To Object ID", Guid.NewGuid());
            employmentRelationship.Add("GUID", Guid.NewGuid());
            return employmentRelationship;
        }

        private static void ValidateObjectAgainstSchema(JObject person, JSchema personSchema)
        {
            if (person.IsValid(personSchema))
            {
                Console.WriteLine("person object is valid according to schema");
            }
            else
            {
                Console.WriteLine("person is not valid according to schema");
            }
        }

        private static void DisplaySchema(JSchema personSchema)
        {
            Console.WriteLine("\nThis is our schema:\n" + personSchema);
        }

        private static JSchema CreatePersonSchema()
        {
            JSchema personSchema = new JSchema();
            //give the template a description and a type ("object" for complex types, otherwise the default JSON types)
            personSchema.Title = "People";
            personSchema.Description = "Schema for a person object";
            personSchema.Type = JSchemaType.Object;

            //define any properties we want
            personSchema.Properties["First Name"] = new JSchema { Type = JSchemaType.String };
            personSchema.Properties["Middle Name"] = new JSchema { Type = JSchemaType.String };
            personSchema.Properties["Last Name"] = new JSchema { Type = JSchemaType.String };
            //TODO - fill out the rest of the schema

            //specify if any properties are required
            personSchema.Required.Add("First Name");

            //specify if additional properties are allowed
            personSchema.AllowAdditionalProperties = true;
            JSchema addressSchema = CreateAddressSchema();

            //specify that "Addresses" property must be an array and only contains objects that match the address schema
            personSchema.Properties["Addresses"] = new JSchema { Type = JSchemaType.Array };
            //specify that the types that the items in this array are allowed to be
            personSchema.Properties["Addresses"].Items.Add(addressSchema);
            //specify that no other item types should be allowed
            personSchema.Properties["Addresses"].AllowAdditionalItems = false;

            //we can also generate a schema directly from a class if we want
            JSchema phoneSchema = CreatePhoneNumberSchema();
            personSchema.Properties["Phone Numbers"] = new JSchema { Type = JSchemaType.Array };
            personSchema.Properties["Phone Numbers"].Items.Add(phoneSchema);
            personSchema.Properties["Phone Numbers"].AllowAdditionalItems = false;
            return personSchema;
        }

        private static JSchema CreatePhoneNumberSchema()
        {
            JSchemaGenerator generator = new JSchemaGenerator();
            var phoneSchema = generator.Generate(typeof(PhoneNumber));
            return phoneSchema;
        }

        private static JSchema CreateAddressSchema()
        {
            JSchema addressSchema = new JSchema();
            addressSchema.Title = "Addresses";
            addressSchema.Description = "Schema for an address object";
            addressSchema.Type = JSchemaType.Object;
            addressSchema.Properties["Line 1"] = new JSchema { Type = JSchemaType.String };
            addressSchema.Properties["City"] = new JSchema { Type = JSchemaType.String };
            //TODO fill out the rest of the schema, then set allowAdditionalProperties to false
            addressSchema.AllowAdditionalProperties = true;
            return addressSchema;
        }

        private static void DisplayPropertiesAndTypes(JObject jo)
        {
            //I can iterate over the properties
            
            foreach (var prop in jo.Properties())
            {
                DisplayProp(prop);
            }
        }

        private static void ConvertAttributes(JObject attr)
        {
            
        }

        private static JObject CreateSamplePerson()
        {
            JObject person = new JObject();
            //I can add any properties to this object, in two different ways
            person["GUID"] = Guid.NewGuid();
            person["SchemaTitle"] = "People";
            person["Display Name"] = "John";

            JObject personName = new JObject();
            personName.Add("First Name", "John");
            personName.Add("Last Name", "Newquist");
            personName["Middle Name"] = "Waldo";

            person["Name"] = personName;

            var phonenumbers = new List<PhoneNumber> { new PhoneNumber { Purpose = "Cell", Number = "770-286-3296" },
                new PhoneNumber { Purpose = "Work", Number = "770-375-4581" } };
            //I can convert C# objects into JObjects, and add those as properties
            person.Add("Phone Numbers", JToken.FromObject(phonenumbers));

            var phones = (JArray)person["Phone Numbers"];
            var firstphone = phones[0];

            //I can add anonymous objects, without needing a class definition
            person.Add("Home Address", JToken.FromObject(new { City = "Norcross", Line1 = "2272 Blacksmith Ct", State = "Georgia" }));

            //I can fill in additional properties on one of my constituent objects
            var homeAddress = person["Home Address"];
            if (homeAddress.Type == JTokenType.Object)
            {
                ((JObject)homeAddress).Add("ZIP", "30071");
            }

            return person;
        }

        private static void PressAnyKeyToContinue()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to exit... wait, which one's the any key?");
            Console.ReadKey();
        }

        private static void DisplayToken(JToken tok, int depth = 0)
        {
            switch(tok.Type)
            {
                case JTokenType.Property:
                    DisplayProp((JProperty)tok, depth);
                    break;
                case JTokenType.Object:
                    Console.WriteLine("{");
                    foreach (var subprop in ((JObject)tok).Properties())
                    {
                        DisplayProp(subprop, depth + 1);
                    }
                    Console.WriteLine("} : object");
                    break;
                default:
                    Console.WriteLine(string.Format("{0} values not handled yet", tok.Type));
                    break;
            }
        }

        private static void DisplayProp(JProperty prop, int depth = 0)
        {
            WriteTabDepth(depth);

            switch (prop.Value.Type)
            {
                case JTokenType.String:
                    Console.WriteLine(string.Format("{0} : \"{1}\" ({2})", prop.Name, prop.Value, prop.Value.Type));
                    break;
                case JTokenType.Integer:
                case JTokenType.Boolean:
                case JTokenType.Date:
                case JTokenType.Float:
                case JTokenType.Guid:
                case JTokenType.TimeSpan:
                    Console.WriteLine(string.Format("{0} : {1} ({2})", prop.Name, prop.Value, prop.Value.Type));
                    break;
                case JTokenType.Array:
                    Console.WriteLine("{0}: [", prop.Name);
                    foreach (var subprop in ((JArray)prop.Value))
                    {
                        DisplayToken(subprop, depth + 1);
                    }
                    Console.WriteLine("] : {0}", prop.Value.Type);
                    break;
                case JTokenType.Object:
                    Console.WriteLine("{0}: {{", prop.Name);
                    foreach (var subprop in ((JObject)prop.Value).Properties())
                    {
                        DisplayProp(subprop, depth + 1);
                    }
                    Console.WriteLine("}} : {0}", prop.Value.Type);
                    break;
                    
                default:
                    Console.WriteLine(string.Format("{0} : {1} ({2} values not handled yet)", prop.Name, prop.Value.Type, prop.Value.Type));
                    break;
            }
        }

        private static void WriteTabDepth(int depth)
        {
            for (int i = 0; i < depth; i++)
            {
                Console.Write("\t");
            }
        }

        private static void SchemaForm(JSchema objectSchema)
        {
            //TODO we might want to put some "header" info above an object
            foreach (var prop in objectSchema.Properties)
            {
                var propName = prop.Key;
                var propType = prop.Value.Type;
                switch (propType)
                {
                    case JSchemaType.Integer:
                        //generate a 
                        break;
                    //TODO handle other cases
                    case JSchemaType.Boolean:
                        //TODO
                        break;
                    case JSchemaType.Number:
                        //TODO
                        break;
                    case JSchemaType.String:
                        //TODO
                        break;
                    case JSchemaType.Array:
                        //TODO if our prop is an array, build a UI that allows multiple elements
                        break;
                    case JSchemaType.Object:
                        //if our prop is an object, we can recursively call or use a helper function to build it
                        SchemaForm(prop.Value);
                        break;
                }
            }
        }

        public class PhoneNumber
        {
            public string Purpose { get; set; }
            public string Number { get; set; }
        }
    }
}
