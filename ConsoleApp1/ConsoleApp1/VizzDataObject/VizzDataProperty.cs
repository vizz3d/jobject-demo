﻿using System.Collections;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

namespace Vizz.core.data
{
    /// <summary>
    /// class which holds a singular data type for each VizzDataObjects
    /// </summary>
    public class VizzDataProperty
    {
        /// <summary>
        /// property type for THIS 
        /// </summary>
        public Type PropertyType { get; set; }

        /// <summary>
        /// enum for data types
        /// </summary>
        public enum Type
        {
            booleanProp,
            integerProp,
            floatProp,
            stringProp,
            file,
            group,
            task,
            //complexProp,
        }

        /// <summary>
        /// name of the property
        /// </summary>
        public string PropertyName { get; set; }

        public JProperty ToJProperty()
        {
            switch (this.PropertyType)
            {
                case VizzDataProperty.Type.stringProp:
                    return new JProperty(this.PropertyName, ((StringProperty)this).Value);

                case VizzDataProperty.Type.booleanProp:
                    return new JProperty(this.PropertyName, ((BoolProperty)this).Value);

                default:
                    throw new NotImplementedException();
            }
        }
    }

    //ANCILLARY CLASSES
    //*****************
    //*****************

    //bool
    public class BoolProperty : VizzDataProperty
    {
        public BoolProperty(string name, bool value)
        {
            this.PropertyName = name;
            this.Value = value;
        }
        public bool Value { get; set; }
    }

    //int
    public class IntegerProperty : VizzDataProperty
    {
        public int Value { get; set; }
    }

    //float
    public class FloatProperty : VizzDataProperty
    {
        public float Value { get; set; }
    }

    //string
    public class StringProperty : VizzDataProperty
    {
        public string Value { get; set; }
    }

    //file
    public class FileProperty : VizzDataProperty
    {
        public byte[] FileByteArray { get; set; }
    }

    //public class ComplexProperty : VizzDataProperty
    //{
    //    public List<VizzDataProperty> Value { get; set; } = new List<VizzDataProperty>();
    //}

    /// <summary>
    /// when an object has a property of GROUP, it's comprised of a bunch of other VizzDataObjects and
    /// is there for the aggregate of their many properties
    /// </summary>
    public class GroupProperty : VizzDataProperty
    {
        /// <summary>
        /// a list of all the names of other templates that comprise a complex grouping
        /// </summary>
        public List<string> templateObjects { get; set; } = new List<string>();
    }

    //task
    /// <summary>
    /// another special class that we want to just implement here but have the more robust part in a different .cs file
    /// this simple implementation is the first step
    /// </summary>
    public partial class TaskProperty : VizzDataProperty
    {
        /// <summary>
        /// priority of task
        /// </summary>
        public Priority MyPriority { get; set; }
        public enum Priority
        {
            high,
            mid,
            low
        }

        /// <summary>
        /// state of task
        /// </summary>
        public State MyState { get; set; }
        public enum State
        {
            open,
            inProgress,
            blocked,
            complete
        }
    }
}


