﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Vizz.core.data
{
    /// <summary>
    /// core raw data object for the application, @author mandrews
    /// </summary>
    [System.Serializable]
    public class VizzDataObject
    {
        //constructors
        public VizzDataObject()
        {
            Properties = new List<VizzDataProperty>();
            SecondaryObjects = new List<VizzDataRelationship>();
        }

        public VizzDataObject(TemplateConfig tempConfig, string objName,
            string templateType = "", string templateParent = "")
        {
            TemplateConfiguration = tempConfig;
            DisplayName = objName;
            TemplateType = templateType;
            TemplateParent = TemplateParent;

            Properties = new List<VizzDataProperty>();
            RelatedObjects = new List<VizzDataRelationship>();
            SecondaryObjects = new List<VizzDataRelationship>();
        }

        /// <summary>
        /// unique id as stored in the LOCAL database. This has no ramifications for the backend
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// who created this object? This should be the local user's owner id
        /// </summary>
        public string ObjectOwner { get; set; }

        public int Frequency { get; set; }

        /// <summary>
        /// this uuid field is the one tracked by the backend
        /// </summary>
        public Guid UUID { get; set; }

        /// <summary>
        /// determines whether the template is user or system created
        /// </summary>
        public TemplateConfig TemplateConfiguration { get; set; }
        public enum TemplateConfig
        {
            system,
            userCreated
        }

        /// <summary>
        /// which type of template is this object modeled after?
        /// </summary>
        public string TemplateType { get; set; }

        /// <summary>
        /// what template (if any) does this template "inherit from"
        /// </summary>
        public string TemplateParent { get; set; }

        /// <summary>
        /// what is THIS object's display name?
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// all properties for this object
        /// </summary>
        public List<VizzDataProperty> Properties { get; set; }

        /// <summary>
        /// list of objects needed to create this template type
        /// </summary>
        public List<string> templateMembers { get; set; }

        /// <summary>
        /// class that lists of all the related vizz objects AND their descriptions
        /// </summary>
        public List<VizzDataRelationship> RelatedObjects { get; set; }
        #region jwn_added
        public List<VizzDataRelationship> SecondaryObjects { get; set; }
        #endregion

        public class VizzDataRelationship
        {
            public VizzDataRelationship()
            {
                RelationDescriptions = new List<string>();
            }

            public string RelationId { get; set; }
            public List<string> RelationDescriptions { get; set; }
            public float Priority { get; set; }
            public float Strength { get; set; }

            #region jwn_added
            public string relationshipName { get; set; }
            public List<VizzDataProperty> relationshipProperties { get; set; } = new List<VizzDataProperty>();

            public VizzDataObject relatedObject { get; set; }
            public string Direction { get; set; }
            public string RelationshipType { get; set; }
            #endregion
        }

        /// <summary>
        /// list of all objects THIS object has access to
        /// </summary>
        public List<int> AvailableObjects { get; set; }

        /// <summary>
        /// list of objects locked to THIS objects 
        /// </summary>
        public List<int> LockedObjects { get; set; }

        /// <summary>
        /// list of all the workspaces this object is a part of
        /// </summary>
        public List<string> WorkSpaces { get; set; }

        public VizzDataProperty GetPropertyByName(string name)
        {
            foreach (VizzDataProperty vdp in Properties)
            {
                if (vdp.PropertyName.Equals(name))
                {
                    return vdp;
                }
            }

            return null;
        }

        /// <summary>
        /// Turns this VizzDataObject into a JObject following the schema set forth here: 
        /// https://vizz3d.atlassian.net/wiki/spaces/V2/pages/273088519/ViZZ+Data+Management+API+Message+Template
        /// (Not confirmed finished as of 5/28)
        /// </summary>
        /// <returns></returns>
        public JObject ToJObject()
        {
            JObject conversion = new JObject();

            JObject attributes = new JObject();
            foreach (var attr in this.Properties)
            {
                attributes.Add(attr.ToJProperty());
            }

            JArray relations = new JArray();
            foreach(var secondary in this.SecondaryObjects)
            {
                JObject rel = new JObject();
                rel["secondaryObject"] = secondary.relatedObject.ToJObject();
                rel["direction"] = secondary.Direction;
                rel["relationType"] = secondary.RelationshipType;
                rel["relationship"] = secondary.relationshipName;
                relations.Add(rel);
            }

            conversion["attributes"] = attributes;
            conversion["relations"] = relations;

            conversion["Id"] = this.Id;
            conversion["templateType"] = this.TemplateType;
            conversion["UUID"] = this.UUID;
            conversion["Workspaces"] = new JArray(this.WorkSpaces);
            conversion["ObjectOwner"] = this.ObjectOwner;
            conversion["Frequency"] = this.Frequency;
            conversion["TemplateConfiguration"] = (int)this.TemplateConfiguration;
            conversion["DisplayName"] = this.DisplayName;

            return conversion;
        }
    }
}
