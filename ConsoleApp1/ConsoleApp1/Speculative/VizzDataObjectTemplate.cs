﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class VizzDataObjectTemplate
    {
        public Guid ID { get; set; }
        public string TemplateName { get; set; }
        public List<TemplateElement> Elements { get; set; }
    }

    public class TemplateElement
    {
        public string ElementName { get; set; }
    }
}
